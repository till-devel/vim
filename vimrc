" Common
set nocompatible
set encoding=utf-8
set t_Co=256

syntax on
filetype plugin on
"let g:molokai_original=1
colorscheme lucius

set background=light
set number
set showcmd
set wildmenu
set confirm

set clipboard=unnamedplus

set textwidth=78
set wrap
"set linebreak

set cursorline

if exists("+columncolor")
	let &colorcolumn=join(range(&textwidth + 1,999),',')
endif

set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab

"set foldmethod=indent

set autoindent
set cindent

" Always show status line
set laststatus=2
set statusline=%<\ %f%(\ [%H%W%R%M]%)\ \%y\ %{&fenc}%=%l,%c\ %P\ 
set noruler

" Enable mouse
set mouse=a

" Search
set nohlsearch
set incsearch
set smartcase
set nowrapscan

" Encoding
set termencoding=utf-8
set fileencodings=utf-8,cp1251,cp866,koi8-r

if !has("gui_running")
	set wcm=<TAB>
	menu Encoding.utf-8 :e ++enc=utf-8<CR>
	menu Encoding.windows-1251 :e ++enc=cp1251 ++ff=dos<CR>
	nmap <F8> :emenu Encoding.<TAB>

	menu SpellLang.none :setlocal nospell<CR>
	menu SpellLang.en :setlocal spell spelllang=en_us<CR>
	menu SpellLang.ru_en :setlocal spell spelllang=ru_yo,en_us<CR>
	nmap <F9> :emenu SpellLang.<TAB>
endif

au! BufWritePost ~/.vimrc source ~/.vimrc

nmap <leader>l :set list!<CR>
set listchars=tab:▸\ ,eol:¬,trail:·

" Mapping
nmap <F2> :w!<CR>
imap <F2> <Esc>:w!<CR>a
vmap <F2> <Esc>:w!<CR>

nmap <F3> :tabnew<CR>
imap <F3> <Esc>:tabnew<CR>
vmap <F3> <Esc>:tabnew<CR>

nmap <F4> :tabprevious<CR>
imap <F4> <Esc>:tabprevious<CR>
vmap <F4> <Esc>:tabprevious<CR>

nmap <F5> :tabnext<CR>
imap <F5> <Esc>:tabnext<CR>
vmap <F5> <Esc>:tabnext<CR>

nmap <F10> :q<CR>
imap <F10> <Esc>:q<CR>
vmap <F10> <Esc>:q<CR>

